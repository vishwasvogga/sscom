/*
	* Copyright (C) 2016 Srishti ESDM Pvt Ltd.
	*
	* This file is property of Srishti ESDM Pvt Ltd and must not be copied, reused or distributed
	* without prior approval and agreement with Srishti ESDM Pvt Ltd.
	*
	* This file is allowed only for Srishti internal use and is a confidential property.
	*/

package com.srishti.sscom.sscom_v3;
/* Author: Hemalatha
* The class gives the status codes for communication status with server
*/
public class SSCOM_StatusCode {	
	public final static short SSCOM_RSP_NONE 		    = -1;		//default status code
	public final static short SSCOM_RSP_GOOD 		    = 10000;	//success
	public final static short SSCOM_RSP_ERR_NW		    = 10001;	//Internet disabled
	public final static short SSCOM_RSP_ERR_TIMEOUT 	= 10002;	//timeout in reaching server
	public final static short SSCOM_RSP_ERR_URL 		= 10003;	//incorrect URL
	public final static short SSCOM_RSP_ERR_SRV_COMM	= 10004;	//Failure comm/ comm rejected due to security reasons
	public final static short SSCOM_RSP_INSECURE_CONN	= 10005;	//Insecure conn attempt
	public final static short SSCOM_RSP_ERR_CER	        = 10006;	//Incorrect certificate keystore file(.bks file)
	public final static short SSCOM_RSP_ERR_ENCRY	    = 10007;	//encryption failure err
	public final static short SSCOM_RSP_NO_URL	   		= 10008;	//No URL given
}
