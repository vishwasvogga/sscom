/*
	* Copyright (C) 2016 Srishti ESDM Pvt Ltd.
	*
	* This file is property of Srishti ESDM Pvt Ltd and must not be copied, reused or distributed
	* without prior approval and agreement with Srishti ESDM Pvt Ltd.
	*
	* This file is allowed only for Srishti internal use and is a confidential property.
	*/
package com.srishti.sscom.sscom_v3;

public class SSCOM_SrvConnProperties {
	/* Author: Hemalatha
	 * Server Configurations properties
	 */
	public String SSCOM_SrvProp_ServerURL 		= "";
	public int  SSCOM_SrvProp_ServerTimeoutMs = 30000;//milliseconds
	public short  SSCOM_SrvProp_ServerRetries 	= 1;

}
