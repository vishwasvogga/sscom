/*
	* Copyright (C) 2016 Srishti ESDM Pvt Ltd.
	*
	* This file is property of Srishti ESDM Pvt Ltd and must not be copied, reused or distributed
	* without prior approval and agreement with Srishti ESDM Pvt Ltd.
	*
	* This file is allowed only for Srishti internal use and is a confidential property.
	*/
package com.srishti.sscom.sscom_v3;
import java.util.HashMap;

/* Author: Hemalatha
* This class gives SSCOM_Response object structure
*/
public class SSCOM_Response {
	public short 	SSCOM_Status 	= SSCOM_StatusCode.SSCOM_RSP_GOOD;
	public HashMap<String, String> SSCOM_httpRspHeader = new HashMap<String, String>();
	public String 	SSCOM_Response 	= "";
}
