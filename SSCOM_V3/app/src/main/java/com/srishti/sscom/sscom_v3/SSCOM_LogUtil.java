package com.srishti.sscom.sscom_v3;

import android.util.Log;

/**Author: Hemalatha
 * Created by Dell on 26-09-2016.
 * This class is used to anable or disable Logs that is used for debugging purposes
 */
public class SSCOM_LogUtil {
    public static void SSCOM_createLogI(String SSCOM_TAG,String SSCOM_val){
        Log.i(SSCOM_TAG,SSCOM_val);
    }
}
