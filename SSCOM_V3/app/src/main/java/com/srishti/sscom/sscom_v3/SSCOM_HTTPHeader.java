/*
	* Copyright (C) 2016 Srishti ESDM Pvt Ltd.
	*
	* This file is property of Srishti ESDM Pvt Ltd and must not be copied, reused or distributed
	* without prior approval and agreement with Srishti ESDM Pvt Ltd.
	*
	* This file is allowed only for Srishti internal use and is a confidential property.
	*/

package com.srishti.sscom.sscom_v3;
/* Author: Hemalatha
* The class gives the structure of SSCOM_HTTPHeader with Request Header FieldName & Request Header FieldValue
*/

public class SSCOM_HTTPHeader {
	public String SSCOM_HTTP_HL_FieldName  = "";
	public String SSCOM_HTTP_HL_FieldValue = "";
}