/*
	* Copyright (C) 2016 Srishti ESDM Pvt Ltd.
	*
	* This file is property of Srishti ESDM Pvt Ltd and must not be copied, reused or distributed
	* without prior approval and agreement with Srishti ESDM Pvt Ltd.
	*
	* This file is allowed only for Srishti internal use and is a confidential property.
	*/

package com.srishti.sscom.sscom_v3;

import android.content.Context;

import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.HttpParams;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.security.KeyStore;

/**Author: Hemalatha
 * Created by Dell on 17-06-2016.
 */
public class SSCOM_MyHttpClient extends DefaultHttpClient {
    private static Context context;

    public static void setContext(Context context) {
        SSCOM_MyHttpClient.context = context;
    }

//    public MyHttpClient(HttpParams params) {
//        super(params);
//    }

    public SSCOM_MyHttpClient() {
        super();
    }

    public SSCOM_MyHttpClient(ClientConnectionManager httpConnectionManager, HttpParams params) {
        super(httpConnectionManager, params);
    }

    @Override
    protected ClientConnectionManager createClientConnectionManager() {
//        Log.i("Test MyHttpClient", "");
        SchemeRegistry registry = new SchemeRegistry();
        registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        // Register for port 443 our SSLSocketFactory with our keystore
        // to the ConnectionManager
        registry.register(new Scheme("https", newSslSocketFactory(), 443));
        return new SingleClientConnManager(getParams(), registry);
    }

    //To create custom SSLsocket
    public static SSLSocketFactory newSslSocketFactory() {
        try {
            // Get an instance of the Bouncy Castle KeyStore format
            KeyStore trusted = KeyStore.getInstance("BKS");
            // Get the raw resource, which contains the keystore with your trusted certificates (root and any intermediate certs)
            //name of your keystore file here
            InputStream SSCOM_bksInStream = new BufferedInputStream(context.getAssets().open("SCIDRClient.bks"));
//            Log.i("BKS Read", ".bks file read successfull");
            try {
                // Initialize the keystore with the provided trusted certificates
                // Provide the password of the keystore
                trusted.load(SSCOM_bksInStream, "srishtiesdm".toCharArray());
            } finally {
                SSCOM_bksInStream.close();
            }
            // Pass the keystore to the SSLSocketFactory. The factory is responsible
            // for the verification of the server certificate.
            SSLSocketFactory SSCOM_socfac = new SSLSocketFactory(trusted);
            // Hostname verification from certificate
            // http://hc.apache.org/httpcomponents-client-ga/tutorial/html/connmgmt.html#d4e506
            SSCOM_socfac.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER); // This can be changed to less stricter verifiers, according to need
//            Log.i("Test MyHttpClient", "Default client created");
            return SSCOM_socfac;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new AssertionError(ex);
        }
    }


}
