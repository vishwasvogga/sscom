/*
	* Copyright (C) 2016 Srishti ESDM Pvt Ltd.
	*
	* This file is property of Srishti ESDM Pvt Ltd and must not be copied, reused or distributed
	* without prior approval and agreement with Srishti ESDM Pvt Ltd.
	*
	* This file is allowed only for Srishti internal use and is a confidential property.
	*/

package com.srishti.sscom.sscom_v3;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import static com.srishti.sscom.sscom_v3.SSCOM_LogUtil.SSCOM_createLogI;

/* Author: Hemalatha
 * This class gives methods to call communication module
 */
public class SSCOM_Communication {
	public static Context SSCOM_mCurrentAndroidContext = null;

	private int SSCOM_mMaxFallbackServers = 0;
	private int SSCOM_mFallbackServerNumber = 0;

	private ArrayList<SSCOM_SrvConnProperties> SSCOM_mSrvProps = null;
	private ArrayList<SSCOM_HTTPHeader> SSCOM_mReqHttpStdHeaders = null;
	private ArrayList<SSCOM_HTTPHeader> SSCOM_mReqHttpCustomHeaders = null;
	private String 						SSCOM_mReqHttpEntityBody = null;

	private SSCOM_Response 				SSCOM_mResponse = null;
	public static String SSCOM_encrypt="";		//encryption required is indicated by value=2
	private String SSCOM_EncryRequired="2";		//This value is defined for encryption at SCIDR server
	public static String SSCOM_RequestType="https";	// this can be http/https, based on URL
	public SSCOM_Communication(Context SSCOM_currentAndroidContext){
		SSCOM_mCurrentAndroidContext 		= SSCOM_currentAndroidContext;
	}

	public SSCOM_Communication() {
		super();
	}

	/*
	* Author: Hemalatha
	* The method sends request and gets response from server.
	* This is the API availed to user to establish comm with server and get response
	* (request compliant with HTTPS servers with self signed certificate only)
	*/
	public SSCOM_Response SSCOM_sendReqGetResp(ArrayList<SSCOM_SrvConnProperties> SSCOM_SrvConnProps,
											   ArrayList<SSCOM_HTTPHeader> SSCOM_httpStdHeaders,
											   ArrayList<SSCOM_HTTPHeader> SSCOM_httpCustomHeaders,
											   String SSCOM_httpEntityBody) {
		SSCOM_mResponse = new SSCOM_Response();
		SSCOM_mResponse.SSCOM_Response = "";
		SSCOM_mResponse.SSCOM_Status   = SSCOM_StatusCode.SSCOM_RSP_NONE;

		if(SSCOM_determineNetworkAvailability()){
			SSCOM_mSrvProps = SSCOM_SrvConnProps;					//server conn props
			SSCOM_mMaxFallbackServers=SSCOM_SrvConnProps.size();	//no. of server conn props
			SSCOM_mReqHttpStdHeaders = SSCOM_httpStdHeaders;		//std http headers
			SSCOM_mReqHttpCustomHeaders = SSCOM_httpCustomHeaders;	//custom http headers
			SSCOM_mReqHttpEntityBody = SSCOM_httpEntityBody;

			if(SSCOM_encrypt.equals(SSCOM_EncryRequired)) {
				String SCIDR_cryptoKey = "sRiShTi#!2016bLr";
				try {
					SCIDR_Crypto SSCOM_crypto = new SCIDR_Crypto(SCIDR_cryptoKey);
					SSCOM_mReqHttpEntityBody = SSCOM_crypto.SCIDR_encrypt(SSCOM_httpEntityBody);
					SSCOM_createLogI("SSCOM", "EncryptionStatus: Success");
				} catch (Exception ex) {
					try{
						SSCOM_mResponse.SSCOM_Status = SSCOM_StatusCode.SSCOM_RSP_ERR_ENCRY;
					}catch(Exception e){
						e.printStackTrace();
						SSCOM_createLogI("SSCOM","SSCOM"+e.getCause());
					}
					SSCOM_createLogI("SSCOM", "EncryptionStatus: Failure");
					ex.printStackTrace();
					SSCOM_createLogI("SSCOM: ","SSCOM"+ex.getCause());
				}
			}
			//to retry with same URLs
			if(SSCOM_mMaxFallbackServers>=1) {
				while (SSCOM_mFallbackServerNumber < SSCOM_mMaxFallbackServers) {
					SSCOM_sendDataToServer();
//				System.out.println("SSCOM_Comm : SrvCount = " + SSCOM_mFallbackServerNumber + ", status = " +SSCOM_mResponse.SSCOM_Status );
					if (SSCOM_mResponse.SSCOM_Status == SSCOM_StatusCode.SSCOM_RSP_GOOD) {
						break;
					}
					SSCOM_mFallbackServerNumber++;
				}
			}else{
				SSCOM_mResponse.SSCOM_Status=SSCOM_StatusCode.SSCOM_RSP_NO_URL;		//No server conn. props given
			}
		}
		else{
			//No internet connection
			SSCOM_createLogI("Network Availability: ","Please Check Your Internet Connection");
			SSCOM_mResponse.SSCOM_Status = SSCOM_StatusCode.SSCOM_RSP_ERR_NW;
		}
		return SSCOM_mResponse;
	}

	/*
	 * This method returns the status of Internet Connection.
	 * @params: None
	 * @return boolean status of Internet Connection
	 */
	private boolean SSCOM_determineNetworkAvailability()  {
		//Android Check of Network Availability using Context
		ConnectivityManager cm = (ConnectivityManager) SSCOM_mCurrentAndroidContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		/**
		 * Determine Internet Connectivity and return status of
		 * Internet connection
		 */
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		boolean isInternetConnected = activeNetwork != null
				&& activeNetwork.isConnectedOrConnecting();
		SSCOM_createLogI("Network Availability: ", isInternetConnected + "");
		return isInternetConnected;
	}

	/**
	 * Author: Hemalatha
	 * Sends data to server over HTTPS post.
	 * The HTTPS standard headers, custom headers and the entity body
	 * are picked up from the input provided to this API
	 * @return response string
	 */
	private String SSCOM_sendDataToServer() {
		SSCOM_createLogI("SSCOM_Comm: sendDataToServer : Sending data to server " ,"URL= "+SSCOM_mSrvProps.get(SSCOM_mFallbackServerNumber).SSCOM_SrvProp_ServerURL);
		String SSCOM_https_url=SSCOM_mSrvProps.get(SSCOM_mFallbackServerNumber).SSCOM_SrvProp_ServerURL;
		try {
			//check if server is HTTPS compliant
			if(SSCOM_RequestType.equalsIgnoreCase("https")) {
				HttpsURLConnection SSCOM_conn = SSCOM_setUpHttpsConnection(SSCOM_https_url);
			}

			//If connection with server is secure, then establish communication
//			if(!SSCOM_conn.equals(null) ) {

				final ServerAsync task = new ServerAsync();
				task.execute(new String[]{SSCOM_mSrvProps.get(SSCOM_mFallbackServerNumber).SSCOM_SrvProp_ServerURL});
				try {
					task.get(SSCOM_mSrvProps.get(SSCOM_mFallbackServerNumber).SSCOM_SrvProp_ServerTimeoutMs, TimeUnit.MILLISECONDS);
				} catch (InterruptedException ex) {
					try{
						task.cancel(true);
						SSCOM_mResponse.SSCOM_Status = SSCOM_StatusCode.SSCOM_RSP_ERR_SRV_COMM;
					}catch(Exception e1){
						e1.printStackTrace();
						SSCOM_createLogI("SSCOM","SSCOM"+e1.getCause());
					}
//					System.out.println("Comm. Interrupted Exception: " + SSCOM_mResponse.SSCOM_Status + "");
					ex.printStackTrace();
					SSCOM_createLogI("SSCOM: ","SSCOM"+ex.getCause());
				} catch (ExecutionException ex) {
					try{
						task.cancel(true);
						SSCOM_mResponse.SSCOM_Status = SSCOM_StatusCode.SSCOM_RSP_ERR_SRV_COMM;
					}catch(Exception e1){
						e1.printStackTrace();
						SSCOM_createLogI("SSCOM","SSCOM"+e1.getCause());
					}
//					System.out.println("Comm. Execution Exception: " + SSCOM_mResponse.SSCOM_Status + "");
					ex.printStackTrace();
					SSCOM_createLogI("SSCOM: ","SSCOM"+ex.getCause());
				} catch (TimeoutException ex) {
					try{
						task.cancel(true);
						SSCOM_mResponse.SSCOM_Status = SSCOM_StatusCode.SSCOM_RSP_ERR_TIMEOUT;
					}catch(Exception e1){
						e1.printStackTrace();
						SSCOM_createLogI("SSCOM","SSCOM"+e1.getCause());
					}
//					System.out.println("Timeout Exception: " + SSCOM_mResponse.SSCOM_Status + "");
				}
//			}else{
//				Log.w("SSCOM:sendDataToServer","Connection is NULL");
//				SSCOM_mResponse.SSCOM_Status = SSCOM_StatusCode.SSCOM_RSP_INSECURE_CONN;
//			}
		}catch(NullPointerException ex){
			try{
				SSCOM_mResponse.SSCOM_Status = SSCOM_StatusCode.SSCOM_RSP_INSECURE_CONN;
			}catch(Exception e1){
				e1.printStackTrace();
				SSCOM_createLogI("SSCOM","SSCOM"+e1.getCause());
			}
			SSCOM_createLogI("Insecure Server","");
			ex.printStackTrace();
			SSCOM_createLogI("SSCOM: ","SSCOM"+ex.getCause());
		}
		catch(Exception ex){
			SSCOM_createLogI("Insecure Server","Connection Failure with Server");
			try{
				SSCOM_mResponse.SSCOM_Status = SSCOM_StatusCode.SSCOM_RSP_INSECURE_CONN;
			}catch(Exception e1){
				e1.printStackTrace();
				SSCOM_createLogI("SSCOM","SSCOM"+e1.getCause());
			}
			ex.printStackTrace();
			SSCOM_createLogI("SSCOM: ","SSCOM"+ex.getCause());
		}
		//test end
		return SSCOM_mResponse.SSCOM_Response;
	}

	/*
	 * Author: Hemalatha
	 * This class is handling background communication task to communicate with the SCIDR
	 */
	class ServerAsync extends AsyncTask<String, String, String> {
		public ServerAsync(){
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			short lCounter = SSCOM_mSrvProps.get(SSCOM_mFallbackServerNumber).SSCOM_SrvProp_ServerRetries;
			while(lCounter > 0){
				try {
					//create custom client to verify server peer with certificate creds
					HttpClient httpclient = createHttpClient();
					HttpPost httppost = new HttpPost(SSCOM_mSrvProps.get(SSCOM_mFallbackServerNumber).SSCOM_SrvProp_ServerURL);
					try {
						StringEntity entity = new StringEntity(SSCOM_mReqHttpEntityBody, "UTF-8");
						httppost.setEntity(entity);
						//Set Standard HTTP Header values
						httppost.addHeader("Accept", "application/xml");//TBD Must change for JSON
						//HTTP standard headers added
						if(SSCOM_mReqHttpStdHeaders != null){
							Iterator<SSCOM_HTTPHeader> it = SSCOM_mReqHttpStdHeaders.iterator();
							while (it.hasNext()) {
								SSCOM_HTTPHeader SSCOM_hTTPHeader = it.next();
								if(SSCOM_hTTPHeader.SSCOM_HTTP_HL_FieldName != null
										&& SSCOM_hTTPHeader.SSCOM_HTTP_HL_FieldValue != null){
									httppost.addHeader(SSCOM_hTTPHeader.SSCOM_HTTP_HL_FieldName,
											SSCOM_hTTPHeader.SSCOM_HTTP_HL_FieldValue);
								}
							}
						}

						//custom HTTP Header values added
						if(SSCOM_mReqHttpCustomHeaders != null){
							Iterator<SSCOM_HTTPHeader> it = SSCOM_mReqHttpCustomHeaders.iterator();
							while (it.hasNext()) {
								SSCOM_HTTPHeader SSCOM_hTTPHeader = it.next();
								if(SSCOM_hTTPHeader.SSCOM_HTTP_HL_FieldName != null
										&& SSCOM_hTTPHeader.SSCOM_HTTP_HL_FieldValue != null){
									httppost.addHeader(SSCOM_hTTPHeader.SSCOM_HTTP_HL_FieldName,
											SSCOM_hTTPHeader.SSCOM_HTTP_HL_FieldValue);

									SSCOM_createLogI("Custom HTTP headers: ",SSCOM_hTTPHeader.SSCOM_HTTP_HL_FieldName +","+SSCOM_hTTPHeader.SSCOM_HTTP_HL_FieldValue);
								}
							}
						}

						HttpResponse execute;
						execute = httpclient.execute(httppost);
						int code = execute.getStatusLine().getStatusCode();
						StringBuilder response=new StringBuilder();
						if(code == 200){
							SSCOM_mResponse.SSCOM_Status = SSCOM_StatusCode.SSCOM_RSP_GOOD;
							//SSCOM_mResponse.SSCOM_Response = EntityUtils.toString(execute.getEntity());
							BufferedReader reader = new BufferedReader(new InputStreamReader(execute.getEntity().getContent()));
							String line;
							while((line = reader.readLine())!= null){
								response.append(line);
							}
							try {
								if (SSCOM_encrypt.equals(SSCOM_EncryRequired)) {
									SSCOM_mResponse.SSCOM_Response = SCIDR_Crypto.SCIDR_decrypt(response.toString());
								} else {
									SSCOM_mResponse.SSCOM_Response = response.toString();
								}
							}catch(Exception ex){
								SSCOM_createLogI("SSCOM:","Err Decryption");
								try{
									SSCOM_mResponse.SSCOM_Status=SSCOM_StatusCode.SSCOM_RSP_ERR_ENCRY;
								}catch(Exception e1){
									e1.printStackTrace();
									SSCOM_createLogI("SSCOM","SSCOM"+e1.getCause());
								}
								ex.printStackTrace();
								SSCOM_createLogI("SSCOM: ","SSCOM"+ex.getCause());
							}
							SSCOM_mResponse.SSCOM_httpRspHeader=SSCOM_getRspHeaders(execute);
							break;
						}
						else if(code == 404){
							SSCOM_createLogI("ErrorURL","Please Check URL");
							SSCOM_mResponse.SSCOM_Status = SSCOM_StatusCode.SSCOM_RSP_ERR_URL;
						}
						else if(code==408 || code==504){
							SSCOM_createLogI("Timeout","Communication Timeout");
							SSCOM_mResponse.SSCOM_Status = SSCOM_StatusCode.SSCOM_RSP_ERR_TIMEOUT;
						}
						else {
							SSCOM_createLogI("Server Comm Err","Error Server Communication");
							SSCOM_mResponse.SSCOM_Status = SSCOM_StatusCode.SSCOM_RSP_ERR_SRV_COMM;
						}
					} catch (HttpHostConnectException ex) {
						SSCOM_createLogI("SSCOM: HttpHostConExcep","");
						try{
							SSCOM_mResponse.SSCOM_Status = SSCOM_StatusCode.SSCOM_RSP_ERR_URL;
						}catch(Exception e1){
							e1.printStackTrace();
							SSCOM_createLogI("SSCOM","SSCOM"+e1.getCause());
						}
						ex.printStackTrace();
						SSCOM_createLogI("SSCOM: ","SSCOM"+ex.getCause());
					}
				}
				catch (IllegalArgumentException e) {
				System.out.println("Untrusted Certificate");
					try{
						SSCOM_mResponse.SSCOM_Status=SSCOM_StatusCode.SSCOM_RSP_ERR_CER;
					}catch(Exception e1){
						e1.printStackTrace();
						SSCOM_createLogI("SSCOM","SSCOM"+e1.getCause());
					}
					e.printStackTrace();
					SSCOM_createLogI("SSCOM","SSCOM"+e.getCause());
				}catch (Exception e) {
					try{
						SSCOM_mResponse.SSCOM_Status = SSCOM_StatusCode.SSCOM_RSP_ERR_SRV_COMM;
					}catch(Exception e1){
						e1.printStackTrace();
						SSCOM_createLogI("SSCOM","SSCOM"+e1.getCause());
					}
					SSCOM_createLogI("SSCOM Exception: ", e.getMessage());
					e.printStackTrace();
					SSCOM_createLogI("SSCOM","SSCOM"+e.getCause());
				}
				lCounter--;
			}
			return SSCOM_mResponse.SSCOM_Response;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
		}


		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			SSCOM_mResponse.SSCOM_Response = result;
			SSCOM_createLogI("Response: ",result);
		}
	}

	/*
	* Author: Hemalatha
	* The method reads response headers
	*  @param HttpResponse object
	*  @return HashMap<String,String> of headers
	 */
	public HashMap<String, String> SSCOM_getRspHeaders(HttpResponse SSCOM_response) {

		Header[] SCIDR_responseHeaders = SSCOM_response.getAllHeaders();
		HashMap<String,String> SSCOM_CustResponseHeaders=new HashMap<String,String>(SCIDR_responseHeaders.length);
		for (Header header : SCIDR_responseHeaders) {
			SSCOM_CustResponseHeaders.put(header.getName(), header.getValue());
		}
		return SSCOM_CustResponseHeaders;

	}

	/* Author: Hemalatha
	* Checks for Https connection, that abide to custom certificate keys
	* @params: URL string
	* @return: HttpsURLConnection
	*/
	private HttpsURLConnection SSCOM_setUpHttpsConnection(String urlString) {
		try {
			// Load CAs from an InputStream(could be from a resource or ByteArrayInputStream or ...)
			CertificateFactory cf = CertificateFactory.getInstance("X.509");

			//Read .cer file
			InputStream caInput = new BufferedInputStream(SSCOM_mCurrentAndroidContext.getAssets().open("SCIDRClient.cer"));
			InputStream SSCOM_keystore = new BufferedInputStream(SSCOM_mCurrentAndroidContext.getAssets().open("SCIDRClient.bks"));
			Certificate ca = cf.generateCertificate(caInput);
//			System.out.println("Custom CA=" + ((X509Certificate) ca).getSubjectDN());

			// Create a KeyStore containing our trusted CAs
			String keyStoreType = "BKS";
			KeyStore keyStore = KeyStore.getInstance(keyStoreType);
			keyStore.load(SSCOM_keystore, "srishtiesdm".toCharArray());
			keyStore.setCertificateEntry("srishtiesdm", ca);		//password of BKS file

			// Create a TrustManager that trusts the CAs in our KeyStore
			String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
			TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
			tmf.init(keyStore);

			// Create an SSLContext that uses our TrustManager
			SSLContext context = SSLContext.getInstance("TLS");
			context.init(null, tmf.getTrustManagers(), null);

			// Tell the URLConnection to use a SocketFactory from our SSLContext
			URL url = new URL(urlString);
			HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
			urlConnection.setSSLSocketFactory(context.getSocketFactory());
//			urlConnection.setRequestMethod("POST");
//			urlConnection.setConnectTimeout(SSCOM_mSrvProps.get(SSCOM_mFallbackServerNumber).SSCOM_SrvProp_ServerTimeoutMs);

			return urlConnection;
		}catch(IOException ex){
			try{
				SSCOM_mResponse.SSCOM_Status = SSCOM_StatusCode.SSCOM_RSP_ERR_CER;
			}catch(Exception e1){
				e1.printStackTrace();
				SSCOM_createLogI("SSCOM","SSCOM"+e1.getCause());
			}
//			Log.w("SSCOM: Exception", ".cer/truststore file Error: " + ex.toString());
			ex.printStackTrace();
			SSCOM_createLogI("SSCOM: ","SSCOM"+ex.getCause());
			return null;
		}
		catch (Exception ex) {
//			Log.w("SSCOM: Exception", "SSL connection Failure: HTTP communication not allowed");
			try{
				SSCOM_mResponse.SSCOM_Status = SSCOM_StatusCode.SSCOM_RSP_INSECURE_CONN;
			}catch(Exception e1){
				e1.printStackTrace();
				SSCOM_createLogI("SSCOM","SSCOM"+e1.getCause());
			}
			ex.printStackTrace();
			SSCOM_createLogI("SSCOM: ","SSCOM"+ex.getCause());
			return null;
		}

	}

	/* Author: Hemalatha
	* To create customHttpClient, that checks for server validity with custom .cer file details
	* to communicate with SCIDR server
	* @params: None
	* @return: HttpClient
	 */
	private static HttpClient createHttpClient(){
		try{
			X509TrustManager x509TrustManager = new X509TrustManager() {
				@Override
				public void checkClientTrusted(X509Certificate[] chain,
											   String authType) throws CertificateException {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] chain,
											   String authType) throws CertificateException {

					InputStream inStream = null;
					X509Certificate[] trustedCerts = new X509Certificate[1];
					try {
						inStream = new BufferedInputStream(SSCOM_mCurrentAndroidContext.getAssets().open("SCIDRClient.cer"));
						CertificateFactory cf = CertificateFactory.getInstance("X.509");
						X509Certificate ca = (X509Certificate)cf.generateCertificate(inStream);
						//add only custom certificate
						trustedCerts[0]=ca;
						for (X509Certificate cert : trustedCerts) {
							// Verifing by public key from custom certificate
							cert.verify(ca.getPublicKey());
						}
					} catch (Exception e) {
						SSCOM_createLogI("SSCOM","HttpClient Creation Failed Due To Untrusted .cer!");
						SSCOM_createLogI("SSCOM","SSCOM"+e.getCause());
						throw new IllegalArgumentException("Untrusted Certificate!");

					} finally {
						try {
							inStream.close();
						} catch (IOException e) {
							SSCOM_createLogI("SSCOM:","Failed to close .cer file");
							e.printStackTrace();
							SSCOM_createLogI("SSCOM","SSCOM"+e.getCause());
						}
					}
				}

				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
			};

			DefaultHttpClient client = new DefaultHttpClient();

			SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, new X509TrustManager[]{(X509TrustManager) x509TrustManager}, null);
			SSLSocketFactory sslSocketFactory = new SSCOM_ExSSLSocketFactory(sslContext);
			sslSocketFactory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			ClientConnectionManager clientConnectionManager = client.getConnectionManager();
			SchemeRegistry schemeRegistry = clientConnectionManager.getSchemeRegistry();
			schemeRegistry.register(new Scheme("https", sslSocketFactory, 443));
			return new DefaultHttpClient(clientConnectionManager, client.getParams());
		} catch (Exception ex) {
			ex.printStackTrace();
			SSCOM_createLogI("SSCOM: ","SSCOM"+ex.getCause());
			return null;
		}
	}
}