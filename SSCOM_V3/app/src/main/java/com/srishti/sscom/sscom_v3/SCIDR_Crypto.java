/*
	* Copyright (C) 2016 Srishti ESDM Pvt Ltd.
	*
	* This file is property of Srishti ESDM Pvt Ltd and must not be copied, reused or distributed
	* without prior approval and agreement with Srishti ESDM Pvt Ltd.
	*
	* This file is allowed only for Srishti internal use and is a confidential property.
	*/
//Package declaration
package com.srishti.sscom.sscom_v3;

//java Standard Package
import android.util.Base64;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;


import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;


/**
 * @author Prakash
 * This class is responsible for Http Body Encryption  and Decryption Using Symmetric Algorithm Advanced 
 * Encryption Standard(AES)
 * For Symmetric Key Refer SCIDR Configuration file.
 */

public class SCIDR_Crypto {

	//Variable Declaration
	private static String SCIDR_cryptoKey;
	private static SecretKeySpec SCIDR_secretKeySpec;
	private static Cipher SCIDR_cipher;


	/**
	 * This constructor initializes all variables 
	 * @param SCIDR_cryptoKey -Symmetric Key Refer SCIDR Configuration file.
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws NoSuchProviderException 
	 */
	public SCIDR_Crypto(String SCIDR_cryptoKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, NoSuchProviderException {
//		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
		this.SCIDR_cryptoKey = SCIDR_cryptoKey;
		SCIDR_secretKeySpec = new SecretKeySpec(this.SCIDR_cryptoKey.getBytes(), "AES");
		SCIDR_cipher = Cipher.getInstance("AES");
		
	}

	/**
	 * This methods takes http body xml String as input and encyrpt this String using AES symmetric algorithm
	 * and returns encryted base64 encoded String http body.
	 * @param SCIDR_data - http body xml  string
	 * @return SCIDR_encryptedData - Encrypted http body xml  String
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public String SCIDR_encrypt(String SCIDR_xmlData) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

		SCIDR_cipher.init(Cipher.ENCRYPT_MODE, SCIDR_secretKeySpec);
		byte[] SCIDR_encryptedByte = SCIDR_cipher.doFinal(SCIDR_xmlData.getBytes());
		String SCIDR_encryptedData = new String(Base64.encodeToString(SCIDR_encryptedByte,Base64.NO_WRAP));
		return SCIDR_encryptedData;
	}
	
	/**
	 * This methods takes encrypted base64 encoded http body xml String as input and decrypt
	 *  this String using AES symmetric algorithm
	 * and returns decrypted  xml http body String.
	 * @param SCIDR_encryptedData - Encrypted base64 encoded http body xml String
	 * @return SCIDR_decryptedText - Decrypted http body xml  String
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public static String SCIDR_decrypt(String SCIDR_encryptedData) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException {

		//SCIDR_encryptedData = SCIDR_encryptedData.replace("\n", "");
		
		SCIDR_cipher.init(Cipher.DECRYPT_MODE, SCIDR_secretKeySpec);
		byte[] SCIDR_encryptedTextByte = Base64.decode(SCIDR_encryptedData,Base64.NO_WRAP);
		byte[] SCIDR_decryptedByte = SCIDR_cipher.doFinal(SCIDR_encryptedTextByte);
		String SCIDR_decryptedText = new String(SCIDR_decryptedByte);
		
		return SCIDR_decryptedText;
	}
	

}
