package com.srishti.sscom.test.TP_sscom_v3;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.srishti.sscom.sscom_v3.SCIDR_Crypto;
import com.srishti.sscom.sscom_v3.SSCOM_Communication;
import com.srishti.sscom.sscom_v3.SSCOM_HTTPHeader;
import com.srishti.sscom.sscom_v3.SSCOM_Response;
import com.srishti.sscom.sscom_v3.SSCOM_SrvConnProperties;
import com.srishti.sscom.sscom_v3.SSCOM_StatusCode;
import com.sscom_v3.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.StringEntity;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

public class TP_SSCOM extends Activity {
	static ProgressDialog SSCOM_pdialog;
	TextView tv_fail, tv_success;
	EditText etv_num;
	Button onclickSSCOM;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		tv_success=(TextView)findViewById(R.id.tv_success);
		tv_success.setVisibility(View.GONE);

		etv_num=(EditText)findViewById(R.id.etv);
		
		tv_fail=(TextView)findViewById(R.id.tv_failure);
		tv_fail.setVisibility(View.GONE);

//		SSCOM_pdialog=new ProgressDialog(TP_SSCOM.this);
//		SSCOM_pdialog.setTitle("Server Communication");
//		SSCOM_pdialog.setMessage("Sending...");
//		SSCOM_pdialog.setCancelable(true);
//
//
		onclickSSCOM=(Button) findViewById(R.id.button1);
		onclickSSCOM.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				SSCOM_pdialog.show();
//				new BGAsync();
				response(v);


			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	 public void response(View v)
	    {

			Integer n=1;
			n=Integer.parseInt(etv_num.getText()+"");
		 	Log.d("[TP_SSCOM]", "SSCOM Library Testing start");
			long startTime=System.currentTimeMillis()/1000;
			Log.d("StartTime: ",startTime+"s");
			for(int i=0;i<n;i++) {
				SSCOM_Response SSCOM_response = TP_SSCOM_xml();
				if (!(SSCOM_response == null)) {
//					SSCOM_pdialog.dismiss();
					tv_success.setVisibility(View.VISIBLE);
					tv_success.setText("Response Status: " + SSCOM_response.SSCOM_Status + "\n\nResponse Headers: \n" + SSCOM_response.SSCOM_httpRspHeader + "\n\nResponse String: \n" + SSCOM_response.SSCOM_Response);
					tv_fail.setVisibility(View.GONE);
				} else {
//					SSCOM_pdialog.dismiss();
					tv_success.setVisibility(View.GONE);
					tv_fail.setVisibility(View.VISIBLE);
				}
			}
			long endTime=System.currentTimeMillis()/1000;
			Log.d("EndTime: ",endTime+"s");
			Log.d("TransacTime: ",(endTime-startTime)+"s");
	    }

	 /**
	 * @return
	 */
	public SSCOM_Response TP_SSCOM_xml(){


		 	// Step 1: Initialize
		 	SSCOM_Response TP_SSCOM_Response = null;
		 	// Step 1.1: Define & set SSCOM_SrvConnProperties for primary, secondary & tertiary Server Connection Properties
		 	SSCOM_SrvConnProperties TP_SSCOM_SrvProp1 = new SSCOM_SrvConnProperties();
		//1: Test Case: Success
		TP_SSCOM_SrvProp1.SSCOM_SrvProp_ServerURL = "https://scidr-test.aapna-seva.in";
//		TP_SSCOM_SrvProp1.SSCOM_SrvProp_ServerURL = "http://122.166.218.213:8080/SCIDR_3.0-V01.02/";

		//2: Test Case: Timeout
// 		TP_SSCOM_SrvProp1.SSCOM_SrvProp_ServerURL = "http://192.168.10.10:8080/SCIDR_3.0-V01.02/";

		//3: Test Case: Error URL
//		TP_SSCOM_SrvProp1.SSCOM_SrvProp_ServerURL = "https://scidr-test.aapna-seva.in/SCIDR_services";

		//4:Test Case: Error Server Communication(Invalid Hostname)
//		TP_SSCOM_SrvProp1.SSCOM_SrvProp_ServerURL = "https://scidrtest-test.aapna-seva.in";

		//5:Test Case: Insecure Communication is for Https comm. only
		// (As both Http and Https is availed, No check on this)

		 	TP_SSCOM_SrvProp1.SSCOM_SrvProp_ServerTimeoutMs = 60000;
		 	TP_SSCOM_SrvProp1.SSCOM_SrvProp_ServerRetries = 1;

		 	SSCOM_SrvConnProperties TP_SSCOM_SrvProp2 = new SSCOM_SrvConnProperties();
		 	TP_SSCOM_SrvProp2.SSCOM_SrvProp_ServerURL = "http://scidr.azurewebsites.net/SrishtiServer3.01/services";
		 	TP_SSCOM_SrvProp2.SSCOM_SrvProp_ServerTimeoutMs = 60000;
		 	TP_SSCOM_SrvProp2.SSCOM_SrvProp_ServerRetries = 2;
		 	
		 	SSCOM_SrvConnProperties TP_SSCOM_SrvProp3 = new SSCOM_SrvConnProperties();
		 	TP_SSCOM_SrvProp3.SSCOM_SrvProp_ServerURL = "https://akua.transactionanalysts.com:444/pideprocess.aspx";
		 	TP_SSCOM_SrvProp3.SSCOM_SrvProp_ServerTimeoutMs = 60000;
		 	TP_SSCOM_SrvProp3.SSCOM_SrvProp_ServerRetries = 1;

		 	//Step 1.1.1: Create an arraylist of SSCOM_SrvConnProperties & add the above set server conn. prop
		 	ArrayList<SSCOM_SrvConnProperties> TP_SSCOM_SrvProp_ArrayList=new ArrayList<SSCOM_SrvConnProperties>(); 
		 	TP_SSCOM_SrvProp_ArrayList.add(TP_SSCOM_SrvProp1);
//		 	TP_SSCOM_SrvProp_ArrayList.add(TP_SSCOM_SrvProp2);
//		 	TP_SSCOM_SrvProp_ArrayList.add(TP_SSCOM_SrvProp3);
		 	
		 	//Step 2: Define & set SSCOM_HTTPHeader parameters, add it to arraylist of HTTPHeaders  
		 	ArrayList<SSCOM_HTTPHeader> SSCOM_mReqHttpCustHeaders = new ArrayList<SSCOM_HTTPHeader>();
		 	SSCOM_HTTPHeader MessageID=new SSCOM_HTTPHeader();
		 	MessageID.SSCOM_HTTP_HL_FieldName="SCIDR_ReqHL_MessageID";
		 	MessageID.SSCOM_HTTP_HL_FieldValue="23";//"30";
		 	SSCOM_mReqHttpCustHeaders.add(MessageID);
		 	
		 	SSCOM_HTTPHeader Source=new SSCOM_HTTPHeader();
		 	Source.SSCOM_HTTP_HL_FieldName="SCIDR_ReqHL_Source";
		 	Source.SSCOM_HTTP_HL_FieldValue="SAMAY";//"License";
		 	SSCOM_mReqHttpCustHeaders.add(Source);
		 	
		 	SSCOM_HTTPHeader Sub_Source=new SSCOM_HTTPHeader();
		 	Sub_Source.SSCOM_HTTP_HL_FieldName="SCIDR_ReqHL_Sub_Source";
		 	Sub_Source.SSCOM_HTTP_HL_FieldValue="GetData";//"ValidityCheck";
		 	SSCOM_mReqHttpCustHeaders.add(Sub_Source);
		 	
		 	SSCOM_HTTPHeader DeviceID_Mfg=new SSCOM_HTTPHeader();
		 	DeviceID_Mfg.SSCOM_HTTP_HL_FieldName="SCIDR_ReqHL_DeviceID_Mfg";
		 	DeviceID_Mfg.SSCOM_HTTP_HL_FieldValue="";//"SRISHTI-ESDM";
		 	SSCOM_mReqHttpCustHeaders.add(DeviceID_Mfg);
		 	
		 	SSCOM_HTTPHeader SCIDR_ReqHL_DeviceID_SrNo=new SSCOM_HTTPHeader();
		 	SCIDR_ReqHL_DeviceID_SrNo.SSCOM_HTTP_HL_FieldName="SCIDR_ReqHL_DeviceID_SrNo";
		 	SCIDR_ReqHL_DeviceID_SrNo.SSCOM_HTTP_HL_FieldValue="SEL/20160401/002/100001";
		 	SSCOM_mReqHttpCustHeaders.add(SCIDR_ReqHL_DeviceID_SrNo);
		 	
		 	SSCOM_HTTPHeader SCIDR_ReqHL_DeviceID_ModelNo=new SSCOM_HTTPHeader();
		 	SCIDR_ReqHL_DeviceID_ModelNo.SSCOM_HTTP_HL_FieldName="SCIDR_ReqHL_DeviceID_ModelNo";
		 	SCIDR_ReqHL_DeviceID_ModelNo.SSCOM_HTTP_HL_FieldValue="";//"SAMAY-ET-2.0";
		 	SSCOM_mReqHttpCustHeaders.add(SCIDR_ReqHL_DeviceID_ModelNo);
		 	
		 	SSCOM_HTTPHeader SCIDR_ReqHL_VersionNo=new SSCOM_HTTPHeader();
		 	SCIDR_ReqHL_VersionNo.SSCOM_HTTP_HL_FieldName="SCIDR_ReqHL_VersionNo";
		 	SCIDR_ReqHL_VersionNo.SSCOM_HTTP_HL_FieldValue="03.01";
		 	SSCOM_mReqHttpCustHeaders.add(SCIDR_ReqHL_VersionNo);

		//Set encryption required/not required (2: Encryption req)
		SSCOM_Communication.SSCOM_encrypt="";	//2: encryption required
		SSCOM_HTTPHeader SCIDR_ReqHL_Encrypt=new SSCOM_HTTPHeader();
		SCIDR_ReqHL_Encrypt.SSCOM_HTTP_HL_FieldName="SCIDR_ReqHL_Encrypt";
		SCIDR_ReqHL_Encrypt.SSCOM_HTTP_HL_FieldValue=SSCOM_Communication.SSCOM_encrypt;
		SSCOM_mReqHttpCustHeaders.add(SCIDR_ReqHL_Encrypt);

		 	


		 	
		 	//Step 3: Get the XML format SCIDR_RequestData2x
			String TP_SSCOM_ReqHttpEntityBody = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?><DataBlock><A1>927462230651</A1><A2></A2><A3></A3><A4></A4><A5></A5><A6></A6><A7></A7><A8>0</A8><A9></A9><A10></A10><A11></A11><A12></A12><A13>0</A13><A14></A14><A15></A15><A16></A16><A17></A17><A18></A18><A19></A19><A20></A20><A21></A21><A22></A22><A23></A23><A24></A24><A25></A25><A26></A26><A27></A27><A28></A28><A29></A29><A30></A30><A31></A31><A32></A32><A33></A33><A34></A34><A35></A35><A36></A36><A37></A37><A38>0</A38><A39></A39><A40></A40><A41></A41><A42></A42><A43></A43><A44></A44><A45></A45><A46></A46><A47>S</A47><A48></A48><A49></A49><A50></A50><A51></A51><A52></A52><A53></A53><A54></A54><A55></A55><A56></A56><A57></A57><A58></A58><A59></A59><A60></A60><A61></A61><A62></A62><A63></A63><A64></A64><A65></A65><A66></A66><A67></A67><A68></A68><A69></A69><A70>0</A70><A71>0</A71><A72>0</A72><A73></A73><A74></A74><A75></A75><A76></A76><A77></A77><A78></A78><A79></A79><A80></A80><A81></A81><A82></A82><A83></A83><A84></A84><A85></A85><A86></A86><A87></A87><A88></A88><A89></A89><A90>0</A90><A91>0</A91><A92>0</A92><A93></A93><A94></A94><A95></A95><A96></A96><A97></A97><A98>0</A98><A99>0</A99><A100>0</A100><A101></A101><A102></A102><A103></A103><A104></A104><A105></A105><A106></A106><A107></A107><A108></A108><A109></A109><A110></A110><A111></A111><A112></A112><A113></A113><A114></A114><A115></A115><A116></A116><A117></A117><A118></A118><A119></A119><A120></A120><A121></A121><A122></A122><A123></A123><A124></A124><A125></A125><A126></A126><A127></A127><A128></A128><A129></A129><A130></A130><A131></A131><A132></A132><A133></A133><A134></A134><A135></A135><A136></A136><A137></A137><A138></A138><A139></A139><A140>0</A140><A141>0</A141><A142>0</A142><A143></A143><A144></A144><A145></A145><A146></A146><A147></A147><A148></A148><A149></A149><A150></A150></DataBlock>";
			//730072610686-bn pal
			//395455998915-sharath
			Log.d("[TP_SSCOM]", "");
			try{
				//Step 4: Send request to server
			    //Server Communication
				SSCOM_Communication.SSCOM_RequestType="http";	//"https" value used for https url
				SSCOM_Communication TP_SSCOM_comm = new SSCOM_Communication(TP_SSCOM.this);
				TP_SSCOM_Response = TP_SSCOM_comm.SSCOM_sendReqGetResp(TP_SSCOM_SrvProp_ArrayList, 
														null, 
														SSCOM_mReqHttpCustHeaders, 
														TP_SSCOM_ReqHttpEntityBody);
				
				Log.d("TP_SSCOM_Resp status", TP_SSCOM_Response.SSCOM_Status+"");
				Log.d("TP_SSCOM_Response", TP_SSCOM_Response.SSCOM_Response);
				Log.d("TP_SSCOM_Resp header", TP_SSCOM_Response.SSCOM_httpRspHeader+"");

				if (!(TP_SSCOM_Response == null)) {
//					SSCOM_pdialog.cancel();
					tv_success.setVisibility(View.VISIBLE);
					tv_success.setText("Response Status: " + TP_SSCOM_Response.SSCOM_Status + "\n\nResponse Headers: \n" + TP_SSCOM_Response.SSCOM_httpRspHeader + "\n\nResponse String: \n" + TP_SSCOM_Response.SSCOM_Response);
					tv_fail.setVisibility(View.GONE);
				} else {
//					SSCOM_pdialog.cancel();
					tv_success.setVisibility(View.GONE);
					tv_fail.setVisibility(View.VISIBLE);
				}

			}catch(NullPointerException ex){
				Log.w("TP_SSCOM: ", "Null Pointer Exception");
			}
			catch(Exception ex){
				Log.i("TP_SSCOM_Result", "Exception");
				ex.printStackTrace();
			}		
			return TP_SSCOM_Response;
	 }


	class BGAsync extends AsyncTask<String, String, String> {
		ProgressDialog SSCOM_pdialog;
		public BGAsync(){
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			//Progress dialog
		}

		@Override
		protected String doInBackground(String... params) {
			SSCOM_Response SSCOM_response = TP_SSCOM_xml();
			return SSCOM_response.SSCOM_Response;
		}


		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			SSCOM_pdialog.dismiss();
		}
	}
}